class EventEmitter {
	constructor() {
		this.events = {}
	
	}
	on(eventName, callback) 
	{
		if (this.events[eventName]){
		this.events[eventName].push(callback);
		} else {
		this.events[eventName] = [callback];
	}
	}
	trigger(eventName, rest)
	{
		if(this.events[eventName]){
			this.events[eventName].every(function(cb) {
                var result = cb.apply(null, rest);
                if (result === false) return false;
                return true;
            });
		}
		
	}
}


const ee = new EventEmitter();
ee.on('change', () => {
	console.log('Hello there!');
});
ee.on('change', () => {
	console.log('Hello there!2');
    return false;
});
ee.on('change', () => {
	console.log('Hello there!3');
});
ee.trigger('change');